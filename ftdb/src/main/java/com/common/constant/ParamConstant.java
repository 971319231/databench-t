/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.constant;

/**
 * 系统参数常量
 *
 */
public class ParamConstant {
	
	public static final String PARAM_INIT_PAGE = "init_page_size";
	
	public static final String PARAM_UPDATE_CUSTOMER = "update_customer_bale";

	public static final String PARAM_UPDATE_MOVE_TRANLOG = "move_tranlog_size";

	public static final String PARAM_START_UNIFORMITY = "start_uniformity";

}
