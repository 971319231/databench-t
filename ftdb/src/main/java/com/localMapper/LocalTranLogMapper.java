/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.localMapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.pojo.TranLogStatsDTO;
import com.pojo.Tranlog;
import com.pojo.TranlogCount;
import com.pojo.TranlogTimeGapDTO;
import com.pojo.TranlogWithOrderDTO;

@Mapper
@Repository
public interface LocalTranLogMapper {
    void insertTranlog(Tranlog tranlog);
	void insertTranlogHistory(@Param("num_count")int num_count,@Param("tranlog_strtime")long tranlog_strtime);

	void deleteTranlog(@Param("num_count")int num_count,@Param("tranlog_strtime")long tranlog_strtime);
	Long countTranlog(Tranlog tranlog);
    List<Integer> findTmcostByTrancfgid(@Param("tranlog_trancfgid") String tranlog_trancfgid, @Param("process_id") String process_id);
    List<TranLogStatsDTO> findTmcostCountByTrancfgid(@Param("tranlog_trancfgid") String tranlog_trancfgid, @Param("process_id") String process_id);
    TranlogTimeGapDTO findMinAndMaxTimeByTrancfgid(@Param("tranlog_trancfgid")String tranlog_trancfgid);
    Integer findTmcostWithSpaceByTrancfgid(@Param("tranlog_trancfgid") String tranlog_trancfgid, @Param("unixStartTime") Long unixStartTime);
    List<TranlogWithOrderDTO> findTmcostWithFilterByTrancfgid(@Param("tranlog_trancfgid") String tranlog_trancfgid, @Param("index") Integer index, @Param("unixStartTime") Long unixStartTime, @Param("process_id") String process_id);
	List<TranlogCount> tranlogCostTimeCount(String tranlog_fld1);
}
