/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.service.ShowConfigService;

@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class ShowConfigController {
	
	@Autowired ShowConfigService showConfigService;
	
	@RequestMapping("/getData")
	@ResponseBody
    public String getDataCfg(HttpServletResponse response) {
        return showConfigService.getDataConfig();
    }
	
	@RequestMapping("/getTranData")
	@ResponseBody
    public String getTranCfg(HttpServletResponse response) {
		return showConfigService.getTranConfig();
    }

}
